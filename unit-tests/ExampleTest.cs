﻿using Dapper;
using ui;
using NUnit.Framework;
using System;

namespace unit_tests
{
    [TestFixture]
    public class ExampleTest
    {
        [Test]
        public void ShouldPass()
        {
            var dbConnection = new Database().GetConnection;
            dbConnection.Open();

            var sql = "SELECT 1";
            var expectedResult = 1;
            var actualResult = dbConnection.QueryFirst<int>(sql);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GetAllOrderedVehiclesShouldPass()
        {
            var searchOrder = new ui.SearchOrder();

            var actualResult = searchOrder.GetAllOrderDetails();
            Assert.IsNotNull(actualResult);
        }

        [Test]
        public void GetOrderedDetailsWithOrderNumberShouldPass()
        {
            var searchOrder = new ui.SearchOrder();
            var expectedResult = 1;
            var orderDetails = searchOrder.GetOrderDetailsWithOrderNumber(expectedResult);
            var result = 0;
            foreach(var orderDetail in orderDetails)
            {
                result = orderDetail.order_number;
            }
            var actualResult = result;    
                        
            Assert.AreEqual(actualResult, expectedResult);
        }

        [Test]
        public void GetOrderNumberFromVehicleNumberShouldPass()
        {
            var searchOrder = new ui.SearchOrder();
            var vehicleNumber = 10;
            var orderDetails = searchOrder.GetOrderNumberFromVehicleNumber(vehicleNumber);
            
            Assert.IsNotNull(orderDetails);
        }

        [Test]
        public void GetOrderDetailsFromOrdersShouldPass()
        {
            var searchOrder = new ui.SearchOrder();
            var orderNumber = 1;
            var orderDetails = searchOrder.GetOrderDetailsFromOrderId(orderNumber);

            Assert.IsNotNull(orderDetails);
        }

        [Test]
        public void UpdateOrderShouldFail()
        {
            var searchOrder = new ui.SearchOrder();
            var orderNumber = 1;
            var orderDetails = searchOrder.GetOrderDetailsFromOrderId(orderNumber);
              
            try
            {
                var result = searchOrder.UpdateOrderDetails(orderDetails);
            }  
            catch(Exception ex)
            {
                Assert.True(true);
            }     
            
            
        }
    }
}