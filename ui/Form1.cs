﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ui
{
    public partial class Form1 : Form
    {
        private DataTable orderDt;
        dynamic orderDetails = new ExpandoObject();
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            if(txt_OrderNumber.Text != null)
            {
                var searchOrder = new ui.SearchOrder();
                var orderDetail = searchOrder.GetOrderDetailsWithOrderNumber(Convert.ToInt32(txt_OrderNumber.Text));
                var table = ui.Helpers.ToDataTable(orderDetail);
                dgv_OrderDetails.DataSource = table;
            }
        }

        private void btn_EditOrderDetails_Click(object sender, EventArgs e)
        {
            var vehicleNumber = 0;
            foreach (DataGridViewRow row in dgv_OrderDetails.Rows)
            {
                if (dgv_OrderDetails.SelectedRows.Count == 1)
                {
                    // get information of Vehicle Id from the row
                    string value = dgv_OrderDetails.SelectedRows[0].Cells[12].Value.ToString();

                    vehicleNumber = Convert.ToInt32(value);
                    var orderDetails = GetOrderDetails(vehicleNumber);
                    orderDt = ui.Helpers.ToDataTable(orderDetails);
                    dgv_OrderEdit.DataSource = orderDt;
                }
            }
        }

        private dynamic GetOrderDetails(int vehicleNumber)
        {
            var searchOrder = new ui.SearchOrder();
            var orderNumber = searchOrder.GetOrderNumberFromVehicleNumber(vehicleNumber);
            return searchOrder.GetOrderDetailsFromOrderId(orderNumber.order_number);
        }

        private void btn_edit_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgv_OrderDetails.Rows)
            {
                if (dgv_OrderEdit.SelectedRows.Count == 1)
                {
                    // get information of Vehicle Id from the row
                    txt_SalePrice.Text = dgv_OrderEdit.SelectedRows[0].Cells[3].Value.ToString();

                    txt_Deposit.Text = dgv_OrderEdit.SelectedRows[0].Cells[4].Value.ToString();

                    orderDetails.order_number = dgv_OrderEdit.SelectedRows[0].Cells[0].Value;
                    orderDetails.sale_price = txt_SalePrice.Text;
                    orderDetails.deposit = txt_Deposit.Text;
                }
            }
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {

            if (txt_SalePrice.Text == null || txt_SalePrice.Text == "0" || txt_SalePrice.Text == "0.00")
            {
                MessageBox.Show("SalePrice should be greater than zero", "Validation");
            }

            if (txt_Deposit.Text == null || txt_Deposit.Text == "0" || txt_Deposit.Text == "0.00")
            {
                MessageBox.Show("Deposit should be greater than zero", "Validation");
            }

            try
            {
                var searchOrder = new ui.SearchOrder();
                searchOrder.UpdateOrderDetails(orderDetails);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Need Permission to edit", "Exception");
            }
            
        }

        private void txt_SalePrice_Validating(object sender, CancelEventArgs e)
        {
           
        }
    }
}
