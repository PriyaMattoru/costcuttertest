﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ui
{
    public class SearchOrder
    {
        public dynamic GetAllOrderDetails()
        {
            using (var dbConnection = new Database().GetConnection)
            {
                dbConnection.Open();

                var sql = "select actual_delivery_date,additional_cost,average_lead_time,base_cost,capacity,colour,ov.engine_designation,expected_delivery_date,fuel_type,ov.model_name,order_number,ov.trim_name,vehicle_number from ordered_vehicles ov,trims t, models m,engines e where ov.model_name=m.model_name and ov.trim_name=t.trim_name and ov.engine_designation = e.engine_designation";

                return dbConnection.Query<object>(sql);
            }
        }

        public dynamic GetOrderDetailsWithOrderNumber(int orderNumber)
        {
            using (var dbConnection = new Database().GetConnection)
            {
                dbConnection.Open();

                var sql = string.Format("select actual_delivery_date,additional_cost,average_lead_time,base_cost,capacity,colour,ov.engine_designation,expected_delivery_date,fuel_type,ov.model_name,order_number,ov.trim_name,vehicle_number from ordered_vehicles ov,trims t, models m,engines e where ov.model_name=m.model_name and ov.trim_name=t.trim_name and ov.engine_designation = e.engine_designation and ov.order_number like {0}", orderNumber);

                return dbConnection.Query<object>(sql);
            }
        }

        public dynamic GetOrderNumberFromVehicleNumber(int vehicleNumber)
        {
            using (var dbConnection = new Database().GetConnection)
            {
                dbConnection.Open();

                var sql = string.Format("select order_number from ordered_vehicles where vehicle_number like {0}", vehicleNumber);

                return dbConnection.QueryFirst<object>(sql);
            }
        }

        public dynamic GetOrderDetailsFromOrderId(int orderNumber)
        {
            using (var dbConnection = new Database().GetConnection)
            {
                dbConnection.Open();

                var sql = string.Format("select order_number,customer_number,employee_number,sale_price,deposit,order_date from orders where order_number like {0}", orderNumber);

                return dbConnection.Query<object>(sql);
            }
               
        }

        public dynamic UpdateOrderDetails(dynamic orderDetails)
        {
            using (var dbConnection = new Database().GetConnection)
            {
                dbConnection.Open();

                foreach (var orderDetail in orderDetails)
                {

                    var sql = @"update orders set sale_price = @sale_price, deposit = @deposit where order_number = @order_number";

                    dbConnection.Execute(sql, new
                    {
                        orderDetail.sale_price,
                        orderDetail.deposit,
                        orderDetail.order_number
                    });
                }

                return GetOrderDetailsFromOrderId(orderDetails.order_number);
            }
        }

    }
}
