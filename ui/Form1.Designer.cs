﻿namespace ui
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_OrderNumber = new System.Windows.Forms.TextBox();
            this.btn_Search = new System.Windows.Forms.Button();
            this.dgv_OrderDetails = new System.Windows.Forms.DataGridView();
            this.btn_EditOrderDetails = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dgv_OrderEdit = new System.Windows.Forms.DataGridView();
            this.btn_edit = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_SalePrice = new System.Windows.Forms.TextBox();
            this.txt_Deposit = new System.Windows.Forms.TextBox();
            this.btn_Update = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_OrderDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_OrderEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Order Number";
            // 
            // txt_OrderNumber
            // 
            this.txt_OrderNumber.Location = new System.Drawing.Point(148, 37);
            this.txt_OrderNumber.Name = "txt_OrderNumber";
            this.txt_OrderNumber.Size = new System.Drawing.Size(186, 20);
            this.txt_OrderNumber.TabIndex = 3;
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(361, 37);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(75, 23);
            this.btn_Search.TabIndex = 6;
            this.btn_Search.Text = "Search";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // dgv_OrderDetails
            // 
            this.dgv_OrderDetails.AllowUserToAddRows = false;
            this.dgv_OrderDetails.AllowUserToDeleteRows = false;
            this.dgv_OrderDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_OrderDetails.Location = new System.Drawing.Point(22, 72);
            this.dgv_OrderDetails.MultiSelect = false;
            this.dgv_OrderDetails.Name = "dgv_OrderDetails";
            this.dgv_OrderDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_OrderDetails.Size = new System.Drawing.Size(932, 101);
            this.dgv_OrderDetails.TabIndex = 7;
            // 
            // btn_EditOrderDetails
            // 
            this.btn_EditOrderDetails.Location = new System.Drawing.Point(361, 196);
            this.btn_EditOrderDetails.Name = "btn_EditOrderDetails";
            this.btn_EditOrderDetails.Size = new System.Drawing.Size(75, 23);
            this.btn_EditOrderDetails.TabIndex = 8;
            this.btn_EditOrderDetails.Text = "OrderDetails";
            this.btn_EditOrderDetails.UseVisualStyleBackColor = true;
            this.btn_EditOrderDetails.Click += new System.EventHandler(this.btn_EditOrderDetails_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(273, 180);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(259, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "(Select the Order before clicking Order Details button)";
            // 
            // dgv_OrderEdit
            // 
            this.dgv_OrderEdit.AllowUserToAddRows = false;
            this.dgv_OrderEdit.AllowUserToDeleteRows = false;
            this.dgv_OrderEdit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_OrderEdit.Location = new System.Drawing.Point(22, 225);
            this.dgv_OrderEdit.MultiSelect = false;
            this.dgv_OrderEdit.Name = "dgv_OrderEdit";
            this.dgv_OrderEdit.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_OrderEdit.Size = new System.Drawing.Size(648, 124);
            this.dgv_OrderEdit.TabIndex = 10;
            // 
            // btn_edit
            // 
            this.btn_edit.Location = new System.Drawing.Point(595, 355);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(75, 23);
            this.btn_edit.TabIndex = 11;
            this.btn_edit.Text = "Edit";
            this.btn_edit.UseVisualStyleBackColor = true;
            this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(676, 241);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Sale Price";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(676, 277);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Deposit";
            // 
            // txt_SalePrice
            // 
            this.txt_SalePrice.Location = new System.Drawing.Point(750, 234);
            this.txt_SalePrice.Name = "txt_SalePrice";
            this.txt_SalePrice.Size = new System.Drawing.Size(159, 20);
            this.txt_SalePrice.TabIndex = 14;
            this.txt_SalePrice.Validating += new System.ComponentModel.CancelEventHandler(this.txt_SalePrice_Validating);
            // 
            // txt_Deposit
            // 
            this.txt_Deposit.Location = new System.Drawing.Point(750, 274);
            this.txt_Deposit.Name = "txt_Deposit";
            this.txt_Deposit.Size = new System.Drawing.Size(159, 20);
            this.txt_Deposit.TabIndex = 15;
            // 
            // btn_Update
            // 
            this.btn_Update.Location = new System.Drawing.Point(834, 300);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(75, 23);
            this.btn_Update.TabIndex = 16;
            this.btn_Update.Text = "Update";
            this.btn_Update.UseVisualStyleBackColor = true;
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(966, 413);
            this.Controls.Add(this.btn_Update);
            this.Controls.Add(this.txt_Deposit);
            this.Controls.Add(this.txt_SalePrice);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_edit);
            this.Controls.Add(this.dgv_OrderEdit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_EditOrderDetails);
            this.Controls.Add(this.dgv_OrderDetails);
            this.Controls.Add(this.btn_Search);
            this.Controls.Add(this.txt_OrderNumber);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_OrderDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_OrderEdit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_OrderNumber;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.DataGridView dgv_OrderDetails;
        private System.Windows.Forms.Button btn_EditOrderDetails;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgv_OrderEdit;
        private System.Windows.Forms.Button btn_edit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_SalePrice;
        private System.Windows.Forms.TextBox txt_Deposit;
        private System.Windows.Forms.Button btn_Update;
    }
}

